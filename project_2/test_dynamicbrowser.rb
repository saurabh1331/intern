require 'rubygems'
require 'data_mapper'
require 'dm-sqlite-adapter'
require 'sinatra'
require 'haml'

DataMapper::Logger.new($stdout, :debug)

DataMapper.setup(:default, "mysql://root:saurabh@localhost/sau")

DataMapper::Property.auto_validation(false)
DataMapper::Property.required(false)


class Leader
        include DataMapper::Resource
        property :id, Serial
        property :lname, String
        has n, :intern, :constraint => :destroy
end

class Intern
        include DataMapper::Resource
        property :id,   Serial  # Required in all DM classes coming from d
        property :name, String, :required => true
        property :college, String
#       property :lname, String
        belongs_to :leader
end



DataMapper.auto_migrate!

DataMapper.finalize

['Gaurav','David'].each do |name|
Leader.create(:lname => name)   
end

[['Pooja',1], ['Saurabh',1], ['nikhil',2], ['bhavya',2]].each do |name|
Intern.create(:name => name[0], :college => 'MNIT' , :leader_id=>name[1])
end


get '/intern' do

@intall = Intern.all
haml :onepage
end

post '/delete' do
@d1 = Intern.first(:name => params[:name]) 
@d1.destroy
@d2 = @d1.name
end

post '/show' do
@foo = Intern.first(:name => params[:name])
@l = Leader.first(:id => @foo.leader_id).lname
@s = @foo.name + " in " + @foo.college + " under " + @l
 #returning @s
end

post '/insert' do
@l1 = Leader.first(:lname=>params[:lname]).id;
Intern.create(:name=>params[:name],:college=>params[:college],:leader_id=>@l1)
@l2 = params[:name]
end

