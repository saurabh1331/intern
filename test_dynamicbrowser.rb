require 'rubygems'
require 'data_mapper'
require 'dm-sqlite-adapter'
require 'sinatra'
require 'haml'

DataMapper::Logger.new($stdout, :debug)

DataMapper.setup(:default, "mysql://root:saurabh@localhost/sau")

DataMapper::Property.auto_validation(false)
DataMapper::Property.required(false)

class Intern
	include DataMapper::Resource
	property :id, 	Serial 	# Required in all DM classes coming from d
	property :name, String, :required => true
	property :college, String
end

class Foo;end

DataMapper.auto_migrate!

DataMapper.finalize

['Pooja', 'Saurabh'].each do |name|
	Intern.create(:name => name, :college => 'MNIT')
end

get '/intern' do

@intall = Intern.all
haml :dynhaml
end

get '/delete' do
haml :delete
end

post '/putdel' do
foo = Intern.first(:name => params[:name])
foo.destroy
end

get '/insert' do
haml :insert 
end

post '/put' do
Intern.create(:name => params[:name], :college => params[:college])

end

get '/:name' do

        @he = Intern.first(:name => params[:name]).name
	@cl = Intern.first(:name => params[:name]).college
    	haml :home

end




