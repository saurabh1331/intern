"use strict";

function appointmentListModel(data){
    var self = this;
	
    this.agent_name = ko.observable(data.agent_name.first_name);
    this.buyer_name = ko.observable(data.buyer);
    this.begin_time = ko.observable(data.begin_time);
    this.end_time = ko.observable(data.end_time);
    this.hash_key = ko.observable(data.hash_key);
    this.time = ko.observable(data.time);
    this.address = ko.observableArray(data.address);
}

function temporaryListModel(data){
    var self = this;
	
    $.extend(this, new appointmentListModel(data));
    
}

function tentativeListModel(data){
    var self = this;
	
    $.extend(this, new appointmentListModel(data));
    
}

function confirmedListModel(data){
    var self = this;
	
    $.extend(this, new appointmentListModel(data));
	
}

var appointmentViewModel;

function AppointmentViewModel() {

    var self = this;
    self.temporaryList = ko.observableArray([]);    
    self.tentativeList = ko.observableArray([]);
    self.confirmedList = ko.observableArray([]);

    self.init = function(){
        if ($("[rel=tooltip]").length){
            $("[rel=tooltip]").tooltip();
        }
    }

    self.showRoute = function(){
        $(".btn-appt-route").click( function() {

            // Find the selector for the scheduled_homes class
            var parent = $(this).parents(".buyer-appt-list");
            var selector = $(parent).find(".scheduled_homes");

            // get those addresses
            var mod_addr_arr = [];
            var ptags=selector.find("p");
            for(var i=0; i<ptags.length;i++){
                mod_addr_arr.push(ptags[i].innerHTML);
            }

            gmapsObj.calcRoute($('#startAddress').val(), $('#endAddress').val(), mod_addr_arr, false);

            return false;
         });        
    }
    
    self.deleteTentative = function(){
        var element = this;
        var appt_data = get_appt_data(element);
        $.post("/appointment/delete",
              appt_data,
              function(data) { delete_callback(data, element, appt_data); }
              );
        }
    
    self.confirmTentative = function(){
        var element = this;
        var appt_data = {};
        appt_data.apptgrouphash = element.hash_key;
        // Post to the server for response from the server
        var buyr = ko.utils.arrayFirst(self.tentativeList(), function(item) {
                                                                       return item.buyer_name ==element.buyer_name; 
                                                                       }
                                      );

        self.confirmedList.push(buyr);
        self.tentativeList.remove(buyr);
        $.post("/appointment/confirm",
               appt_data,
               function(data) {
                   if(data.status == "success"){
                       appointmentViewModel.confirmCallback(appt_data);
                   }
               }
               );
    }
    
    self.confirmCallback = function(appt_data){
        
    }
    
    self.inviteSuggested = function(){
        var element = this;
        var appt_data = {};
        appt_data.apptgrouphash = element.hash_key;
        var buyr = ko.utils.arrayFirst(self.temporaryList(), function(item) {
                                                                       return item.buyer_name ==element.buyer_name; 
                                                                       }
                                      );
        self.tentativeList.push(buyr);
        self.temporaryList.remove(buyr);
        // Post to the server for response from the server
        $.post("/appointment/invite",
               appt_data,
               function(data) {
                   if(data.status == "success"){
                       appointmentViewModel.inviteCallback(appt_data);
                   }
               }
               );
    }
    
    self.inviteCallback = function(appt_data){

    }

    self.showSchedule = function(begin_time, end_time){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        var begin = new Date(begin_time);
        var end = new Date(end_time);
        
        var begin_hours = begin.getHours();
        var begin_ampm = begin_hours >= 12 ? 'pm' : 'am';
        begin_hours = begin_hours % 12;
        begin_hours = begin_hours ? begin_hours : 12;
        
        var end_hours = end.getHours();
        var end_ampm = end_hours >= 12 ? 'pm' : 'am';
        end_hours = end_hours % 12;
        end_hours = end_hours ? end_hours : 12;        
        
        var time = days[begin.getDay()] + " from " + begin_hours + " " +begin_ampm+ " " +"to " + end_hours + " " +end_ampm;
        return time;
        
    }		
    

    
    self.ShowApptsByDate = function(){ 
    var outputDiv = $("#schedulerResultId")[0];
    var new_item = {};
    var j;
    new_item.address = ko.observableArray();
    var currentbuyer=" ";    
    var date = $("#appt-date-start").val();
        if(date){
            $.post("/appointment/show",
                  {date: date},
                  function(data) {  
                      if (data.status === "ok") {
                          // empty the lists each time show button is clicked
                          self.temporaryList.removeAll();
                          self.tentativeList.removeAll();
                          self.confirmedList.removeAll();

                         // loop to show all tentative list
                          var lengthTentativeList = data.htmlstr.tentatives.length; 
                          var k=0;
                          for(var i = 0; i < lengthTentativeList; i++){
                              if(data.htmlstr.tentatives[i].buyer.first_name!=currentbuyer)
                              {  
                                  if(i>=1){
                                      self.tentativeList.push(new_item);
                                      new_item.address=[];
                                  }
                                  currentbuyer = data.htmlstr.tentatives[i].buyer.first_name;
                                  new_item.buyer_name = data.htmlstr.tentatives[i].buyer.first_name+" "+data.htmlstr.tentatives[i].buyer.last_name;
                                  new_item.agent_name = data.htmlstr.tentatives[i].agent.first_name;
                                  new_item.begin_time = data.htmlstr.tentatives[i].begin;
                                  new_item.end_time = data.htmlstr.tentatives[i+3].end;
                                  new_item.time = self.showSchedule(new_item.begin_time, new_item.end_time);
                                  new_item.hash_key =data.htmlstr.tentative_hash_keys[k];
                                  k=k+1;
                              }
                              new_item.address.push(data.htmlstr.tentatives[i].home.street+","+data.htmlstr.tentatives[i].home.city);
                              new_item.end = data.htmlstr.tentatives[i].end;							  
                          }
                          self.tentativeList.push(new_item);
                          new_item = {};
                          new_item.address = ko.observableArray();  
 
                          // loop to show all temporary/suggested list 
                          var lengthTemporaryList = data.htmlstr.temporaries.length; 
                          k=0;
                          for(var i = 0; i < lengthTemporaryList; i++){
                              if(data.htmlstr.temporaries[i].buyer.first_name!=currentbuyer)
                              {     
                                  if(i>=1){
                                      self.temporaryList.push(new_item);
                                      new_item.address=[];
                                  }
                                  currentbuyer = data.htmlstr.temporaries[i].buyer.first_name;
                                  new_item.buyer_name = data.htmlstr.temporaries[i].buyer.first_name+" "+data.htmlstr.temporaries[i].buyer.last_name;
                                  new_item.agent_name = data.htmlstr.temporaries[i].agent.first_name;
                                  new_item.begin_time = data.htmlstr.temporaries[i].begin;
                                  new_item.end_time = data.htmlstr.temporaries[i+3].end;
                                  new_item.time = self.showSchedule(new_item.begin_time, new_item.end_time);
                                  new_item.hash_key =data.htmlstr.temporary_hash_keys[k];
                                  k=k+1;
                              }
                              new_item.address.push(data.htmlstr.temporaries[i].home.street+","+data.htmlstr.temporaries[i].home.city);
                              new_item.end = data.htmlstr.temporaries[i].end;	
			                
                          }
                          self.temporaryList.push(new_item);
                          new_item = {};
                          new_item.address = ko.observableArray();
                             
                          // loop to show confirmed list		 
                          var lengthConfirmedList = data.htmlstr.confirmeds.length; 
                          k=0;
                          for(var i = 0; i < lengthConfirmedList; i++){
                              if(data.htmlstr.confirmeds[i].buyer.first_name!=currentbuyer)
                              {
                                 
                                  if(i>=1){
                                      self.confirmedList.push(new_item);
                                      new_item.address=[];
                                  }
                                  currentbuyer = data.htmlstr.confirmeds[i].buyer.first_name;
                                  new_item.buyer_name = data.htmlstr.confirmeds[i].buyer.first_name+" "+data.htmlstr.confirmeds[i].buyer.last_name;
                                  new_item.agent_name = data.htmlstr.confirmeds[i].agent.first_name;
                                  new_item.begin_time = data.htmlstr.confirmeds[i].begin;
                                  new_item.end_time = data.htmlstr.confirmeds[i+3].end;
                                  new_item.time = self.showSchedule(new_item.begin_time, new_item.end_time);
                                  new_item.hash_key =data.htmlstr.confirmed_hash_keys[k];
                                  k=k+1;
                              }
                              new_item.address.push(data.htmlstr.confirmeds[i].home.street+","+data.htmlstr.confirmeds[i].home.city);
                              new_item.end = data.htmlstr.confirmeds[i].end;	
			                 
                          }
                          self.confirmedList.push(new_item);
                          new_item = {};
                          new_item.address = ko.observableArray();
 		                          				  
                      } else {
                          outputDiv.innerHTML = "<p>" + data.status + "</p>";
                      }
                      init_appointment_ui_objects();
                      var mod_addr_arr = getInnerHTMLAddr('#schedulerResultId .scheduled_homes.active');
                      gmapsObj.calcRoute($('#startAddress').val(), $('#endAddress').val(), mod_addr_arr, false);
                  
                  }
                  );
		}
    };
    
    
};

appointmentViewModel = new AppointmentViewModel();
ko.applyBindings(appointmentViewModel);
appointmentViewModel.init();
